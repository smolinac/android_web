package qa.automated.web.bci.Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class PageVisitasLogin {
	
	/** OBJETOS **/
	
	@FindBy(id  = "cl.bci.app.personas.qa:id/acb_enter")
	private WebElement btnConfir;

	@FindBy(id  = "cl.bci.app.personas.qa:id/et_user_rut")
	private WebElement loginRut;
	
	@FindBy(id = "cl.bci.app.personas.qa:id/et_user_password")
	private WebElement loginPass;
	
	@FindBy(id = "cl.bci.app.personas.qa:id/text_input_password_toggle")
	private WebElement btnEye;
	
	@FindBy(id = "cl.bci.app.personas.qa:id/btn_login")
	private WebElement btnLogin;
	
	@FindBy(id  = "cl.bci.app.personas.qa:id/sucursales_button")
	private WebElement confirmSucursales;//Sucursales

	@FindBy(id  = "cl.bci.app.personas.qa:id/forgotten_password_link")
	private WebElement saludoLogin;//¿Problemas con tu clave?
	
	@FindBy(id  = "cl.bci.app.personas.qa:id/tyc_title")
	private WebElement textoCondiciones;//Términos y condiciones
	
	@FindBy(id = "cl.bci.app.personas.qa:id/accept_terms_checkbox")
	private WebElement chkTerminos;
	
	@FindBy(id = "cl.bci.app.personas.qa:id/btn_accept")
	private WebElement btnTerminos;
	
	@FindBy(id = "cl.bci.app.personas.qa:id/textView5")
	private WebElement textoBienvenidoPrueba;
	
	@FindBy(id = "cl.bci.app.personas.qa:id/btn_begin")
	private WebElement btnVersionPruebaComenzar;

	/** GETTERS Y SETTERS **/
	
	public WebElement getBtnConfir() {
		return btnConfir;
	}

	public void setBtnConfir(WebElement btnConfir) {
		this.btnConfir = btnConfir;
	}

	public WebElement getLoginRut() {
		return loginRut;
	}

	public void setLoginRut(WebElement loginRut) {
		this.loginRut = loginRut;
	}

	public WebElement getLoginPass() {
		return loginPass;
	}

	public void setLoginPass(WebElement loginPass) {
		this.loginPass = loginPass;
	}

	public WebElement getBtnEye() {
		return btnEye;
	}

	public void setBtnEye(WebElement btnEye) {
		this.btnEye = btnEye;
	}

	public WebElement getBtnLogin() {
		return btnLogin;
	}

	public void setBtnLogin(WebElement btnLogin) {
		this.btnLogin = btnLogin;
	}
	
	public WebElement getConfirmSucursales() {
		return confirmSucursales;
	}

	public void setConfirmSucursales(WebElement confirmSucursales) {
		this.confirmSucursales = confirmSucursales;
	}

	public WebElement getSaludoLogin() {
		return saludoLogin;
	}

	public void setSaludoLogin(WebElement saludoLogin) {
		this.saludoLogin = saludoLogin;
	}

	public WebElement getTextoCondiciones() {
		return textoCondiciones;
	}

	public void setTextoCondiciones(WebElement textoCondiciones) {
		this.textoCondiciones = textoCondiciones;
	}
	
	public WebElement getChkTerminos() {
		return chkTerminos;
	}

	public void setChkTerminos(WebElement chkTerminos) {
		this.chkTerminos = chkTerminos;
	}

	public WebElement getBtnTerminos() {
		return btnTerminos;
	}

	public void setBtnTerminos(WebElement btnTerminos) {
		this.btnTerminos = btnTerminos;
	}
	
	public WebElement getTextoBienvenidoPrueba() {
		return textoBienvenidoPrueba;
	}

	public void setTextoBienvenidoPrueba(WebElement textoBienvenidoPrueba) {
		this.textoBienvenidoPrueba = textoBienvenidoPrueba;
	}
	
	public WebElement getBtnVersionPruebaComenzar() {
		return btnVersionPruebaComenzar;
	}

	public void setBtnVersionPruebaComenzar(WebElement btnVersionPruebaComenzar) {
		this.btnVersionPruebaComenzar = btnVersionPruebaComenzar;
	}
	
	/** METODOS **/
	
	public void msgCon() {
		btnConfir.click();
	}
	
	public void ingresarRut(String rut) {
		loginRut.clear();
		loginRut.sendKeys(rut);
	}
	
	public void ingresarClave(String clave) {
		loginPass.clear();
		loginPass.sendKeys(clave);
	}
	
	public void ingresar() {
		btnLogin.click();
	}
	
	public boolean validarRut(String rut)
	{
	    boolean validacion = false;
	    if(rut != null && rut.trim().length() > 0)
	    {
	        try {
	            rut = rut.replaceAll("[.]", "").replaceAll("-", "").trim().toUpperCase();
	            char dv = rut.charAt(rut.length() - 1);
	            String mantisa = rut.substring(0, rut.length() - 1);
	            if( isInteger( mantisa ) )
	            {
	                int mantisaInt = Integer.parseInt( mantisa );
	                validacion = validareRut( mantisaInt, dv ) ;
	            }
	        }
	        catch (Throwable e) 
	        {
	        }
	    }
	    return validacion;
	}

	private boolean validareRut(int rut, char dv)
	{
	    int m = 0, s = 1;
	    for (; rut != 0; rut /= 10)
	    {
	        s = (s + rut % 10 * (9 - m++ % 6)) % 11;

	    }
	    return Character.toUpperCase(dv) == (char) (s != 0 ? s + 47 : 75) ;
	}

	public boolean isInteger(String cad)
	{
	    for(int i = 0; i<cad.length(); i++){
	        if( !Character.isDigit(cad.charAt(i)) )
	        {
	            return false;
	        }
	    }
	    return true;
	}
	
	public String obtenerRut() {
		String valor="";
		try {
			valor = loginRut.getAttribute("text").toString().trim();
		} catch (Exception e) {
			valor = "";
		}
		return valor;
	}
	
	public boolean btnLoginActivo() {
		boolean res = false;
		try {
			res = btnLogin.isDisplayed();
		} catch (Exception e) {
			res = false;
		}
		return res;
	}
	
	public boolean VerActivo() {
		boolean res = false;
		try {
			res = btnEye.isDisplayed();
		} catch (Exception e) {
			res = false;
		}
		return res;
	}
	
	public boolean btnTerminosActivo() {
		boolean res = false;
		try {
			res = btnTerminos.isDisplayed();
		} catch (Exception e) {
			res = false;
		}
		return res;
	}
	
	public void ClickBtnVer() {
		btnEye.click();
	}
	
	public void clickChkTerminos() {
		chkTerminos.click();
	}
	
	public void clickBtnAceptarTerminos() {
		btnTerminos.click();
	}
	
	public void clickCampoRut() {
		loginRut.click();
	}
	
	public void clickBtnVersionPrueba() {
		btnVersionPruebaComenzar.click();
	}
	
	public String textoSucursales() {
		return confirmSucursales.getText().trim();
	}
	
	public String textoProblemas() {
		return saludoLogin.getText();
	}
	
	public String textoCondiciones() {
		return textoCondiciones.getText();
	}
	
	public String textoComenzarVersionPrueba() {
		return textoBienvenidoPrueba.getText();
	}

}
