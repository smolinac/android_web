package qa.automated.web.bci.Properties;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;


public class PropertiesInit {
	public static Properties parametros;
	
    private String SaludoBienvenida;
    private String bci_url;
    private String browser;
    private String selenium_server_url;
    private String browser_local_driver;
    public String version;
	public String platformName;
	public String appPackage;
	public String appPackageBci;
	public String appActivity;
	public String appActivityBci;
	public String URL_APPIUM;
	public String PORT_APPIUM;
	public String DEVICE_NAME;
	public String DEVICE_TYPE;
	public String SALUDOSUCURSAL;
	public String SALUDOLOGIN;
	public String SALUDOCONDICIONES;
	public String SALUDOVERSIONPRUEBA;
    
	public PropertiesInit() {
		Properties parametros;
		File propFileName = new File("src/test/java/qa/automated/web/bci/Config/config.properties").getAbsoluteFile();;
		FileInputStream inputStream;

		
		try {
			inputStream = new FileInputStream(propFileName);
			parametros = new Properties();
			parametros.load(inputStream);

		    bci_url = parametros.getProperty("BCI_URL");
		    browser = parametros.getProperty("BROWSER");
		    selenium_server_url = parametros.getProperty("SELENIUM_SERVER_URL");
		    browser_local_driver = parametros.getProperty("BROWSER_LOCAL_DRIVER");
			SaludoBienvenida = parametros.getProperty("SaludoBienvenida");
			
			// Configuraciones Appium
						version = parametros.getProperty("version");
						platformName = parametros.getProperty("platformName");
						appPackage = parametros.getProperty("appPackage");
						appPackageBci = parametros.getProperty("appPackageBci");
						appActivity = parametros.getProperty("appActivity");
						appActivityBci = parametros.getProperty("appActivityBci");
						URL_APPIUM = parametros.getProperty("URL_APPIUM");
						PORT_APPIUM = parametros.getProperty("PORT_APPIUM");
						DEVICE_NAME = parametros.getProperty("DEVICE_NAME");
						DEVICE_TYPE = parametros.getProperty("DEVICE_TYPE");
						
			// Textos
						SALUDOSUCURSAL = parametros.getProperty("SALUDOSUCURSAL");
						SALUDOLOGIN = parametros.getProperty("SALUDOLOGIN");
						SALUDOCONDICIONES = parametros.getProperty("SALUDOCONDICIONES");
						SALUDOVERSIONPRUEBA = parametros.getProperty("SALUDOVERSIONPRUEBA");
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Modifica fichero de propiedades
	 * 
	 * @param fichero
	 *            Archivo properties a modificar
	 * @param key
	 *            Nombre de properties a modificar
	 * @param value
	 *            Valor que se modificara en propertie
	 * 
	 * @return void
	 */

	
	
    
	public String getSaludoBienvenida() {
		return SaludoBienvenida;
	}

	public String getDEVICE_NAME() {
		return DEVICE_NAME;
	}

	public void setDEVICE_NAME(String dEVICE_NAME) {
		DEVICE_NAME = dEVICE_NAME;
	}

	public String getPORT_APPIUM() {
		return PORT_APPIUM;
	}

	public void setPORT_APPIUM(String pORT_APPIUM) {
		PORT_APPIUM = pORT_APPIUM;
	}

	public String getURL_APPIUM() {
		return URL_APPIUM;
	}

	public void setURL_APPIUM(String uRL_APPIUM) {
		URL_APPIUM = uRL_APPIUM;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getPlatformName() {
		return platformName;
	}

	public void setPlatformName(String platformName) {
		this.platformName = platformName;
	}

	public String getAppPackage() {
		return appPackage;
	}

	public void setAppPackage(String appPackage) {
		this.appPackage = appPackage;
	}

	public String getAppPackageBci() {
		return appPackageBci;
	}

	public void setAppPackageBci(String appPackageBci) {
		this.appPackageBci = appPackageBci;
	}

	public String getAppActivity() {
		return appActivity;
	}

	public void setAppActivity(String appActivity) {
		this.appActivity = appActivity;
	}

	public String getAppActivityBci() {
		return appActivityBci;
	}

	public void setAppActivityBci(String appActivityBci) {
		this.appActivityBci = appActivityBci;
	}

	public void setSaludoBienvenida(String saludoBienvenida) {
		SaludoBienvenida = saludoBienvenida;
	}

	public static Properties getParametros() {
		return parametros;
	}

	public static void setParametros(Properties parametros) {
		PropertiesInit.parametros = parametros;
	}

	public String getBci_url() {
		return bci_url;
	}

	public void setBci_url(String bci_url) {
		this.bci_url = bci_url;
	}

	public String getBrowser() {
		return browser;
	}

	public void setBrowser(String browser) {
		this.browser = browser;
	}

	public String getSelenium_server_url() {
		return selenium_server_url;
	}

	public void setSelenium_server_url(String selenium_server_url) {
		this.selenium_server_url = selenium_server_url;
	}

	public String getBrowser_local_driver() {
		return browser_local_driver;
	}

	public void setBrowser_local_driver(String browser_local_driver) {
		this.browser_local_driver = browser_local_driver;
	}

	public String getDEVICE_TYPE() {
		return DEVICE_TYPE;
	}

	public void setDEVICE_TYPE(String dEVICE_TYPE) {
		DEVICE_TYPE = dEVICE_TYPE;
	}
}
