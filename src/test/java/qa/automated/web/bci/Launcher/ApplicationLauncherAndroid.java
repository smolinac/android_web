package qa.automated.web.bci.Launcher;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.service.local.AppiumServiceBuilder;
import qa.automated.web.bci.Generic.UsoCom;
import qa.automated.web.bci.Pages.PageVisitasLogin;
import qa.automated.web.bci.Properties.PropertiesInit;

import qa.automated.web.bci.Runnings.RunningPageObjetMobileLogin6023;


@RunWith(Suite.class)
@SuiteClasses({ RunningPageObjetMobileLogin6023.class})
public class ApplicationLauncherAndroid {	

	public static AndroidDriver<AndroidElement> driver;
	public static WebDriver driverWeb;
	public static PropertiesInit properties;
	public static final String CUR_DIR = System.getProperty("user.dir");
//	private static AppiumServerJava appium;
	
	// PÁGINAS DE LA WEB
	//public static PageLoginWeb pageLoginWeb;
	//public static PageMobileLogin pageMobileLogin;
	
	//Pagina Mobile
	public static PageVisitasLogin pageVisitasLogin;
	
	private final static String APPIUM_SERVER_URL = "http://127.0.0.1:4780/wd/hub";
	private static AppiumDriverLocalService appium;
	@BeforeClass
	public static void setUp() {
		
		properties = new PropertiesInit();
//      Inicializamos el servidor APPIUM
//		appium = new AppiumServerJava();
//		appium.startServer();
		
		appium = AppiumDriverLocalService.buildService(new AppiumServiceBuilder()
                .withAppiumJS(new File("C:\\Users\\camilo.guerrero\\AppData\\Roaming\\npm\\node_modules\\appium\\build\\lib\\main.js"))
                .usingPort(4780).withIPAddress("127.0.0.1"));
          appium.start();
		
		try {
			if (System.getenv("SELENIUM_SERVER_URL") != null && !System.getenv("SELENIUM_SERVER_URL").equalsIgnoreCase(""))
			if (properties.getSelenium_server_url() != null && !properties.getSelenium_server_url().equalsIgnoreCase("")){
				setDriver();	
			}else{
				setDriverDesa();
			}
			


			// En este caso apunta a la direccion que entrega Appium, el cual esta enlazado al dispositivo
//			driver = new AndroidDriver<AndroidElement>(new URL(System.getProperty("URL_APPIUM")), appium.capabilities());
			driver = new AndroidDriver<AndroidElement>(new URL(properties.getURL_APPIUM()),capabilities());
			//driver.context("WEBVIEW"); 
			driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			//driver.manage().timeouts().pageLoadTimeout(120, TimeUnit.SECONDS);

			// WEB
			//pageLoginWeb = PageFactory.initElements(driverWeb, PageLoginWeb.class);
			//Mobile
			pageVisitasLogin = PageFactory.initElements(driver, PageVisitasLogin.class);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@AfterClass
	public static void finish() {
		System.out.println("************************************************");
		System.out.println("**   Finaliza ciclo de pruebas automatizadas  **");
		System.out.println("************************************************");
		System.out.println();
	    UsoCom.cerrarVentana(driverWeb);
	    appium.stop();
		UsoCom.cerrarDriver(driver);
	    
	}

	public static void setDriver() throws MalformedURLException {
		DesiredCapabilities capability = null;
		
		String browserName = properties.getBrowser();

		if (browserName.equalsIgnoreCase("chrome")){
			capability = DesiredCapabilities.chrome();
		}
		if (browserName.equalsIgnoreCase("IE")){
			capability = DesiredCapabilities.internetExplorer();
			capability.setCapability(InternetExplorerDriver.ENABLE_PERSISTENT_HOVERING, false);
			capability.setCapability(InternetExplorerDriver.REQUIRE_WINDOW_FOCUS, true);
			capability.setCapability(InternetExplorerDriver.UNEXPECTED_ALERT_BEHAVIOR, true);
			capability.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
		}
		if (browserName.equalsIgnoreCase("firefox")){
			capability = DesiredCapabilities.firefox();
			capability.setCapability("marionette", true);
		}
		else{
			capability = DesiredCapabilities.chrome();

		}

		capability.setJavascriptEnabled(true);
		capability.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
		driverWeb = new RemoteWebDriver(new URL(properties.getSelenium_server_url()), capability);
		driverWeb.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driverWeb.manage().timeouts().pageLoadTimeout(120, TimeUnit.SECONDS);
	}

	public static void setDriverDesa() throws MalformedURLException {
		System.setProperty("webdriver.chrome.driver", CUR_DIR + properties.getBrowser_local_driver());
		driverWeb = new ChromeDriver();
		driverWeb.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driverWeb.manage().timeouts().pageLoadTimeout(120, TimeUnit.SECONDS);
		driverWeb.manage().window().maximize();
	}
	
	public static DesiredCapabilities capabilities() {
		DesiredCapabilities capabilities = new DesiredCapabilities();
		
	 //path to eclipse project
	   File rootPath = new File(System.getProperty("user.dir"));
	   //path to folder where the apk is going to be
	   File appDir = new File(rootPath,"/src/test/java/qa/automated/web/bci/apk");
	   //name of the apk
	   //File app = new File(appDir,"app-Staging.apk");
	   File app = new File(appDir,"app-qa (1).apk");
	   //the absolute local path to the APK
	   capabilities.setCapability("app", app.getAbsolutePath());
//UiAutomator2
		capabilities.setCapability("automationName", "UiAutomator2");
	    capabilities.setCapability("deviceName", "ZY223TGBXW");
	   
		//capabilities.setCapability("automationName", "appium");
		//capabilities.setCapability("deviceName", "02157df2b459b902");
		
		//capabilities.setCapability("deviceName",properties.getSerialDispo());
		// capabilities.setCapability(CapabilityType.BROWSER_NAME, "Android");
		// Ruta de APK a instalar
//		capabilities.setCapability("app", System.getenv("APP_INSTALL"));
		
		//capabilities.setCapability(CapabilityType.VERSION, "6.0");
		capabilities.setCapability("platformName", "Android");
		//capabilities.setCapability("platformVersion", "7"); 
//		capabilities.setCapability("appPackage", scotiapackage);
		 capabilities.setCapability("appPackage","cl.bci.app.personas.qa");
		
		 //Despliegue aplicacion
		 capabilities.setCapability("appActivity", "cl.bci.app.personas.presentation.splash.SplashActivity");

		 //cl.bci.sismo.mach.staging-1.apk
		//cl.bci.mach.machapp.utils.appbase.GenericActivity
		capabilities.setCapability("unicodeKeyboard", true);
		capabilities.setCapability("resetKeyboard", true);
		// Tiempo en segundos antes de esperar a cerrar sesion por inactividad
		// para este caso se configuro 30min
		capabilities.setCapability("newCommandTimeout", 1800);
		capabilities.setCapability("noReset", false);
//		capabilities.setCapability("fullReset", false);
		//driver.resetApp ();
		return capabilities;

	}

	public static String getAppiumServerUrl() {
		return APPIUM_SERVER_URL;
	}
}
