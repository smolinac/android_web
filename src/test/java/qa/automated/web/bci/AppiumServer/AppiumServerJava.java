package qa.automated.web.bci.AppiumServer;

import java.io.IOException;
import java.net.ServerSocket;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.service.local.AppiumServiceBuilder;
import io.appium.java_client.service.local.flags.GeneralServerFlag;
import qa.automated.web.bci.Properties.PropertiesInit;


public class AppiumServerJava {


		private AppiumDriverLocalService service;
		public static PropertiesInit properties;

		/**
		 * Configuracion e iniciacion de servidor Appium
		 * 
		 * @return Voids
		 **/
		public void startServer() {
			// Configuracion appium server
			AppiumServiceBuilder builder = new AppiumServiceBuilder();
	        //builder.withIPAddress("127.0.0.1");
			builder.usingPort(4723);
			//builder.withCapabilities(ApplicationLauncher.getD
			builder.withArgument(GeneralServerFlag.SESSION_OVERRIDE);
			builder.withArgument(GeneralServerFlag.LOG_LEVEL, "error");

			// Inicia servicio appium server
			service = AppiumDriverLocalService.buildService(builder);
			service.start();
		}

		/**
		 * Detiene el servidor Appium
		 * 
		 * @return Void
		 **/
		public void stopServer() {
			service.stop();
		}

//		/**
//		 * Configuracion de DesiredCapabilities que se entregaran a appium para que
//		 * se ejecute
//		 * 
//		 * @return DesiredCapabilities
//		 **/
//
//		public DesiredCapabilities capabilities() {
//			DesiredCapabilities capabilities = new DesiredCapabilities();
//			
//		 //path to eclipse project
//		   File rootPath = new File(System.getProperty("user.dir"));
//		   //path to folder where the apk is going to be
//		   File appDir = new File(rootPath,"/src/test/java/qa/automated/web/bci/apk");
//		   //name of the apk
//		   //File app = new File(appDir,"app-Staging.apk");
//		   File app = new File(appDir,"ScotiabankGo_cl.scotiabank.go.apk");
//		   //the absolute local path to the APK
//		   capabilities.setCapability("app", app.getAbsolutePath());
//
//			capabilities.setCapability("automationName", "UiAutomator2");
//		    capabilities.setCapability("deviceName", "RSGUJZEEL79DGA55");
//		   
//			//capabilities.setCapability("automationName", "appium");
//			//capabilities.setCapability("deviceName", "02157df2b459b902");
//			
//			//capabilities.setCapability("deviceName",properties.getSerialDispo());
//			// capabilities.setCapability(CapabilityType.BROWSER_NAME, "Android");
//			// Ruta de APK a instalar
////			capabilities.setCapability("app", System.getenv("APP_INSTALL"));
//			
//			//capabilities.setCapability(CapabilityType.VERSION, "6.0");
//			capabilities.setCapability("platformName", "Android");
//			//capabilities.setCapability("platformVersion", "7"); 
////			capabilities.setCapability("appPackage", scotiapackage);
//			 capabilities.setCapability("appPackage","cl.scotiabank.go");
//			
//			 //Despliegue aplicacion
//			 capabilities.setCapability("appActivity", "cl.scotiabank.scotiabankgo.splash.view.activity.SplashActivity");
//
//			 //cl.bci.sismo.mach.staging-1.apk
//			//cl.bci.mach.machapp.utils.appbase.GenericActivity
//			capabilities.setCapability("unicodeKeyboard", true);
//			capabilities.setCapability("resetKeyboard", true);
//			// Tiempo en segundos antes de esperar a cerrar sesion por inactividad
//			// para este caso se configuro 30min
//			capabilities.setCapability("newCommandTimeout", 1800);
//			capabilities.setCapability("noReset", true);
////			capabilities.setCapability("fullReset", false);
//			//driver.resetApp ();
//			return capabilities;
//
//		}
			
			
	
		
		
		/**
		 * Comprueba si puerto esta en uso, si est� en uso lo libera, no realiza
		 * nada en caso contrario
		 * 
		 * @param int
		 *            port
		 * 
		 * @return true esta en uso el puerto, false no esta en uso o se libero
		 *         puerto
		 **/
		public static boolean checkIfServerIsRunnning(int port) {
			boolean isServerRunning = false;
			ServerSocket serverSocket;
			try {
				serverSocket = new ServerSocket(port);
				serverSocket.close();
			} catch (IOException e) {
				isServerRunning = true;
			} finally {
				serverSocket = null;
			}
			return isServerRunning;
		}

	}